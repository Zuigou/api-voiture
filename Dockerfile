# Utilisez une image de base avec OpenJDK
FROM openjdk:12-alpine

# Copiez le fichier JAR construit dans l'image
COPY ./build/libs/cars-api.jar /app/cars-api.jar

# Exposez le port sur lequel votre application écoute
EXPOSE 5000

# Commande pour exécuter votre application
CMD ["java", "-jar", "/app/cars-api.jar"]
